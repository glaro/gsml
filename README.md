# GSML

## Generalidades sobre machine learning (Esquema introductorio, no para "producción")

## Perú: Biociencias e Investigación en el Bicentenario -- 29 de agosto de 2021


Los archivos corresponden a una *miniguía* sobre una pequeña introducción a *machine learning*. Esto es parte de una charla perteneciente al evento mencionado arriba.

Sobre esta charla, el objetivo principal es mostrar cómo aplicar *machine learning* sin que esto cause lágrimas.

Se incluye un archivo Rmd para ejecutar con R. Además, una base de datos que se utiliza como ejemplo. También se incluyen las diapositivas empleadas en la charla.

Las funciones mostradas acá son solo guías. Dentro del archivo Rmd se da mayor información.


------

This is an encrypted message.

